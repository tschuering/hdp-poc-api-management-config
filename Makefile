default:
	echo "Use explicit target"

kickstarter:
	ls -al $$(pwd)/static
	docker run -it --rm -v $$(pwd):/var/portal-api \
		-e LOCAL_UID=$(id -u) \
		-e LOCAL_GID=$(id -g) \
		-p 3333:3333 haufelexware/wicked.portal-kickstarter
		
update:
	git add .
	git commit -m "updates"
	git push
